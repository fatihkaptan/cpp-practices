//============================================================================
// Name        : 18_functions1.cpp
// Author      : Fatih Kaptan
// Date		   : 17 May 2021 18:11
// Description : Intro to Functions in C++
//============================================================================

#include <iostream>
using namespace std;

//"void" meaning, this funciton does NOT include any output!

void say_Hi();   			//function prototype

void factorial(int input);	//funciton prototype with argument.



int main() {   				//  <--- main function

	say_Hi(); 				// function call


	int number;
	cout << "enter the value that calculate factorial: ";
	cin >> number;
	factorial(number);

	return 0;
}

void say_Hi() { 					//define special function
	cout << "Hellooo!" << endl;
	cout << "WHatsupp?" << endl;
}

void factorial(int input) {
	int fact = 1;
	for (int i = 1; i <= input; i++) {
		fact = i * fact;
	}
	cout << "result= " << fact << endl;
}
