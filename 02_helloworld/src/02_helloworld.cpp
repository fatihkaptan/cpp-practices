//============================================================================
// Name        : 02_helloworld.cpp
// Author      : Fatih Kaptan
// Date        : 16 May 2021 13:08
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main(){

	cout << "Merhaba Dünya!\n"; //  \n -> next line(inline)
	cout << "Merhaba Dünya 2"
	<<endl;  					// endl -> next line command(outline)
	cout << "Merhaba Dünya 3";

	return 0;
}
