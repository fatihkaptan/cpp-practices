//============================================================================
// Name        : 28_structures_withPointers.cpp
// Author      : Fatih Kaptan
// Date		   : 20 May 2021 13:45
// Description : Using pointers with structures in C++
//============================================================================

#include <iostream>
using namespace std;
struct Employee { //define structure
	int id;
	string name;
	string department;
};

int main() {

	Employee employee1;
	employee1.id = 12;
	employee1.name = "fatih kaptan";
	employee1.department = "kontrol müh.";

	Employee* ptr = &employee1; //defining address of structure element

	cout << employee1.name <<endl;

	cout << ptr->department <<endl; //calling object

	return 0;
}
