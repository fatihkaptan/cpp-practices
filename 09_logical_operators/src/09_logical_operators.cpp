//============================================================================
// Name        : 09_if_elseif_else_2.cpp
// Author      : Fatih Kaptan
// Date 	   : 16 May 2021 15:39
// Description : Logical Operators in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

/*
  * OR operator  || ---> true if at least one of the operands is true.
  * AND operator && ---> true only if all the operands are true.
  * NOT operator !  ---> true only if the operand is false.
*/

	string sys_username= "fatihkaptan";
	string sys_password= "12345";

	string username,password;   			//user input

	cout << "Enter the Username: ";
	cin >> username;
	cout <<"Enter the password: ";
	cin >> password;

	if(sys_username == username && sys_password == password){
		 cout << "Welcome :)"<<endl;
	}

	else if(sys_username != username && sys_password == password){
		cout << "Username incorrect!" << endl;
	}

	else if(sys_username == username && sys_password != password){
		cout << "Password incorrect!" <<endl;
	}

	else{
		cout << "Username and Password incorrect!" <<endl;
	}



	return 0;
}
