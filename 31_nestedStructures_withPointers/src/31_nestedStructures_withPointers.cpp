//============================================================================
// Name        : 31_nestedStructures_withPointers.cpp
// Author      : Fatih Kaptan
// Date		   : 20 May 2021 14:09
// Description : Using Nested Structures with Pointers in C++
//============================================================================

#include <iostream>
using namespace std;
struct Address {
	string cityName;
	int No;
};

struct Employee {
	int id;
	string name;
	string department;
	Address* address;  // <--- POINTER defined
};

int main() {

	Employee employee;
	employee.id = 12;
	employee.name = "fatih kaptan";
	employee.department = "kontrol müh.";

	Address address2 = {"Izmır",28};

	employee.address = &address2;

	Employee* ptr = &employee;

	cout << ptr->address->cityName << endl;
	cout << ptr->address->No << endl;



	return 0;
}
