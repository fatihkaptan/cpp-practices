//============================================================================
// Name        : 50_static_variables.cpp
// Author      : Fatih Kaptan
// Date 	   : 31 May 2021 14:00
// Description : Static Variables in C++
//============================================================================

#include <iostream>
using namespace std;


void test() {
	int i = 3;
	i++;
	cout << "i(in function): " << i << endl;
	//note= in this function, "i" variable remove after the function.
}

void test2() {
	static int j = 3;
	j++;
	cout << "j(in function): " << j << endl;
	//note= in this function, "j" variable NOT remove after the function.
}

int main() {

	test();
//	cout << "i(out function):" << i << endl;   //error
	test(); //same
	test(); //same
	cout << "-------------------" << endl;
	test2();
	test2(); //different
	test2(); //different


	return 0;
}
