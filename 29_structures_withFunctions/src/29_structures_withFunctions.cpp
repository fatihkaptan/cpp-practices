//============================================================================
// Name        : 29_structures_withFunctions.cpp
// Author      : Fatih Kaptan
// Date		   : 20 May 2021 13:49
// Description : Using strucures with functions in C++
//============================================================================

#include <iostream>
using namespace std;
// 2 example -> 1. normal parameters, 2. using pointers
struct Employee { //define structure
	int id;
	string name;
	string department;
};

void showEmployee(Employee employee); //prototype
void show2(Employee* employee); //prototype

int main() {

	Employee employee1 = {12,"fatih kaptan", "kontrol müh."};

	showEmployee(employee1);
	cout << " next Id: " << employee1.id << endl; //same
//********up: call by value, down call by reference************
	show2(&employee1);
	cout << " last Id: " << employee1.id << endl; //different

	return 0;
}

void showEmployee(Employee employee){
	cout << "Id: " << employee.id << endl;
	cout << "Name: " << employee.name << endl;
	cout << "Department: " << employee.department <<endl;

	employee.id = 40; //call by value
}

void show2(Employee* employee){
		cout << "Id: " << employee->id << endl;
		cout << "Name: " << employee->name << endl;
		cout << "Department: " << employee->department <<endl;

		employee->id =30; //call by reference
}
