//============================================================================
// Name        : 51_static_members.cpp
// Author      : Fatih Kaptan
// Date 	   : 31 May 2021 14:11
// Description : Static class members in C++
//============================================================================

#include <iostream>
using namespace std;

class Gamer {
public:
	static int gamers; //create static gamers integer that accessable in class
	Gamer() {//define mepty constructor
		gamers++; //increment one every contstruct
		cout << "New gamer created!" << endl;
	}
};
//Note= Do NOT assign value static int _value = 112, assign like below.
int Gamer::gamers = 0; //
int main() {

	Gamer gamer1;
	Gamer gamer2;
	Gamer gamer3;
	Gamer gamer4;
	cout << "Gamers: " << Gamer::gamers << endl;

	return 0;
}
