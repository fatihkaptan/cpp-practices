//============================================================================
// Name        : 52_static_methods.cpp
// Author      : Fatih Kaptan
// Date 	   : 31 May 2021 14:19
// Description : Static Class Methods in C++
//============================================================================

#include <iostream>
using namespace std;
class Math{
public:
	static void cube(int x){ //create static methods
		cout << "result: " << x*x*x <<endl;
	}

	static void add2(int x){
		cout << "result: " << x+2 <<endl;
	}
};

int main() {
	Math::add2(10);
	Math::cube(5);


	return 0;
}
