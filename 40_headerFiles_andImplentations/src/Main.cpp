//============================================================================
// Name        : 40_headerFiles_andImplentations.cpp
// Author      : Fatih Kaptan
// Date 	   : 21 May 2021 13:22
// Description : Header files and implemenatitons in C++
//============================================================================

#include <iostream>
using namespace std;

#include "Header.h" //include header file which contain classes

int main() {

	Employee employee1;
	employee1.id = 12;
	employee1.name = "Fatih";
	employee1.salary = 3000;

	employee1.showInfos();


	return 0;
}
