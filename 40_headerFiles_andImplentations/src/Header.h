/* ---->>> HEADER FILE
 * Header.h
 *
 *  Created on: 21 May 2021
 *      Author: Fatih Kaptan
 */

#ifndef HEADER_H_
#define HEADER_H_
#include <iostream> //add manually !!
using namespace std; //add manually !!

class Employee{
public:
	string name;
	int id;
	int salary;

	void showInfos(); // ex: this method print info. of employees.
	//just prototype, define another file.(on source file Employee.cpp)
};



#endif /* HEADER_H_ */
