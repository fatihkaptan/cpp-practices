//============================================================================
// Name        : 53_friend_functions_andClasses.cpp
// Author      : Fatih Kaptan
// Date 	   : 31 May 2021 14:24
// Description : Friend functions and classes in C++
//============================================================================

#include <iostream>
using namespace std;

class Employee {
private:
	string name;
	int age;
	int salary;
public:
	Employee(string name, int age, int salary) {
		this->name = name;
		this->age = age;
		this->salary = salary;
	}
	friend void showInfos(Employee employee); //define friend function given below.
	friend class Test; //define friend class given below.//define
};

void showInfos(Employee employee) { //do not access directly(because private) class with out-function, define friend func.
	cout << employee.name << " " << employee.age << " " << employee.salary
			<< endl;
}

class Test {
public:
	static void ShowInfos2(Employee employee) {
		cout << employee.name << " " << employee.age << " " << employee.salary
				<< endl;
	}

};
int main() {
	Employee employee("Fatih Kaptan", 587, 3300);
	showInfos(employee); //access private variables using friend function

	Employee employee2("Sabri Abi", 58, 4300);
	Test::ShowInfos2(employee2); //access private variables using friend class
	return 0;
}
