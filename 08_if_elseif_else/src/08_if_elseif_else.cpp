//============================================================================
// Name        : 08_if_elseif_else.cpp
// Author      : Fatih Kaptan
// Date		   : 16 May 2021 14:32
// Description : If, else if , else statements in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int a, b;
	string islem;

	cout << "Calculator the Primitive" << endl;
	cout << "1. Addition(+)" << endl;
	cout << "2. Subtraction(-)" << endl;
	cout << "3. Multiplication(*)" << endl;
	cout << "4. Division(/)" << endl;

	cout << "Which operation? : ";
	cin >> islem;

	if (islem =="1") {
		cout << "a: ";
		cin >> a;
		cout << "b: ";
		cin >> b;

		cout << "a+b= " << a + b << endl;
	} else if (islem == "2") {
		cout << "a: ";
		cin >> a;
		cout << "b: ";
		cin >> b;

		cout << "a-b= " << a - b << endl;
	} else if (islem == "3") {
		cout << "a: ";
		cin >> a;
		cout << "b: ";
		cin >> b;

		cout << "a*b= " << a * b << endl;
	} else if (islem == "4") {
		cout << "a: ";
		cin >> a;
		cout << "b: ";
		cin >> b;

		cout << "a/b= " << a / b << endl;
	} else {
		cout << "unknown operation!";
	}

	return 0;
}
