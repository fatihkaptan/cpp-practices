//============================================================================
// Name        : 16_multidimensional_arrays.cpp
// Author      : Fatih Kaptan
// Date		   : 17 May 2021 17:35
// Description : Multidimensional Arrays in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

//***Printing matrix****

/*
	int matrix[3][3] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };//3x3 matrix (two dimensional array)
	//cout << matrix[0][0];

	for(int i=0; i<3 ; i++){
		for(int j=0 ; j<3 ; j++){
			cout <<  matrix[i][j] << " " ;
		}
		cout << endl;
	}
*/

//***Input Matrix****
	int matrix[3][3];
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			cout << "enter the matrix -> [" << i << "][" << j << "] : ";
			cin >> matrix[i][j];
		}
		cout << endl;
	}

	for(int i=0; i<3 ; i++){
			for(int j=0 ; j<3 ; j++){
				cout <<  matrix[i][j] << " " ;
			}
			cout << endl;
		}


	return 0;
}
