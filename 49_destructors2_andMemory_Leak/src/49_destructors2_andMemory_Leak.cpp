//============================================================================
// Name        : 49_destructors2_andMemory_Leak.cpp
// Author      : Fatih Kaptan
// Date 	   : 31 May 2021 13:47
// Description : Destructors example and Memory Leak in C++
//============================================================================

#include <iostream>
using namespace std;

class Employee {
public:
	string *s;
	int *i;

	Employee(string str, int ivalue){
		s = new string;
		i = new int;

		*s =str;
		*i = ivalue;
	}

	void show(){
		cout <<"String: " << *s << "  |  Int: " << *i <<endl;
	}

	Employee() {
		cout << "Constructor Called!" << endl;
	}

	~Employee() {
		delete s;
		delete i;
		//Note!= if don't delete i&s pointers, memory leakage exist.

		cout << "Destructor Called!" << endl;
	}
};

int main() {

	Employee *emp = new Employee("Fatih Kaptan",587);
	emp->show();
	delete emp; //we also delete pointers in destructor
	emp->show(); //and there is no leakage in memory.

	return 0;
}
