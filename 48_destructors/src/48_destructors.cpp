//============================================================================
// Name        : 48_destructors.cpp
// Author      : Fatih Kaptan
// Date 	   : 31 May 2021 13:39
// Description : Destructors in C++
//============================================================================

#include <iostream>
using namespace std;

class Employee{
public:
	Employee(){
		cout << "Constructor Called!" <<endl;
	}

	~Employee(){
		cout << "Destructor Called!" <<endl;
	}
};

int main() {

	Employee *emp = new Employee(); //create object => call contructor

	delete emp; //delete object after process => call destructor


	return 0;
}
