//============================================================================
// Name        : 38_ObjectOrientedProgramming1.cpp
// Author      : Fatih Kaptan
// Date	  	   : 21 May 2021 13:08
// Description : Intro to Object oriented programming in C++
//============================================================================

#include <iostream>
using namespace std;

class Student{ //define class
public: //specify class for access from everywhere
	string name;

};

int main() {

	Student student1; //create object
	student1.name = "Fatih";

	Student student2;
	student2.name = "Sabri";

	cout << "Student1 Name: " << student1.name <<endl;
	cout << "Student2 Name: " << student2.name <<endl;





	return 0;
}
