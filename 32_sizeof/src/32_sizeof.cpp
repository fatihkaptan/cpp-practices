//============================================================================
// Name        : 32_sizeof.cpp
// Author      : Fatih Kaptan
// Date		   : 20 May 2021 14:21
// Description : Using sizeof operator in C++
//============================================================================

/*The sizeof() is an operator that evaluates the
  size of data type, constants, variable.
  It is a compile-time operator as it returns the size of
  any variable or a constant at the compilation time.
 */

#include <iostream>
using namespace std;

struct Student{
	int id;
	char letter;

};

int main() {

	int int_A = 10;
	char char_A= 'B';
	float float_A = 3.2;
	double double_A = 3.14159;


	cout << "Integer: " << sizeof(int_A) <<endl; //4 byte= 32 bit
	cout << "Char: " << sizeof(char_A) <<endl; //1 byte (in ASCII)
	cout << "Float: " << sizeof(float_A) << endl; // 4 byte
	cout << "Double: " << sizeof(double_A) << endl; // 8 byte
	cout << "Struct: " << sizeof(Student) << endl; //8 byte-changeable



	return 0;
}
