//============================================================================
// Name        : 24_sendArray_toFunciton.cpp
// Author      : Fatih Kaptan
// Date 	   : 19 May 2021 14:52
// Description : Sending arrays to functions example in C++
//============================================================================

#include <iostream>
using namespace std;

void printArray(int array[], int size); //function prototype
void printArray2(int *ptr, int size); //function with pointers prototype

int main() {

	int a[] = { 1, 2, 3, 4, 5 };
	printArray(a, 5);
	cout << "--------" << endl;
	printArray2(a, 5);

	return 0;
}

void printArray(int array[], int size) {
	for (int i = 0; i < size; i++) {
		cout << array[i] << endl;
	}}

void printArray2(int *ptr, int size) {
	for (int i = 0; i < size; i++) {
		cout << ptr[i] << endl;
	}}
