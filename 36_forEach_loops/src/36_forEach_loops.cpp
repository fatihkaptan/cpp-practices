//============================================================================
// Name        : 36_forEach_loops.cpp
// Author      : Fatih Kaptan
// Date	 	   : 21 May 2021 12:56
// Description : For(Each) loops in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int a[] = {10,20,30,40,50};

/*	for(int i =0;i<5;i++){
		cout << a[i] <<endl;
	}
*/
	//FOR EACH MORE QUICK ->>
		for(int item:a){
		cout << item <<endl;
	}
	return 0;
}
