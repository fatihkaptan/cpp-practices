//============================================================================
// Name        : 14_system_login_example.cpp
// Author      : Fatih Kaptan
// Date		   : 16 May 2021 16:48
// Description : User Login Example with break,loops,statements...
//============================================================================

#include <iostream>
using namespace std;

int main() {

	string sys_username = "fatihkaptan";
	string sys_password = "123456";

	string username,password;

	while(true){  //infinite if we cannot use break
		cout << "Enter username: ";
		cin >> username;
		cout << "Enter password: ";
		cin >> password;

		if(username == sys_username && password==sys_password){
			cout << "Welcome :)  " << username << endl;
			break;
		}
		else if(username != sys_username && password==sys_password){
			cout << "Incorrect username"<< endl;
		}
		else if(username == sys_username && password!=sys_password){
			cout << "Incorrect password"<< endl;
		}
		else{
			cout << "Incorrect username&password"<< endl;
		}
	}

	return 0;
}
