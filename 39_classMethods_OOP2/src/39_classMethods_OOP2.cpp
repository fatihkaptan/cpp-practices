//============================================================================
// Name        : 39_classMethods_OOP2.cpp
// Author      : Fatih Kaptan
// Date	  	   : 21 May 2021 13:17
// Description : Class methods in C++
//============================================================================

#include <iostream>
using namespace std;
class Student{
public:
	string name;

	void sayName(){ //Create method in class
		cout << "My name is " << name <<endl;
	}

};

int main() {

	Student student1;
	student1.name = "Fatih";

	student1.sayName();

	Student student2;
	student2.name = "Sabri";
	student2.sayName();


	return 0;
}
