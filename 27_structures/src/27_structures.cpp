//============================================================================
// Name        : 27_structures.cpp
// Author      : Fatih Kaptan
// Date		   : 20 May 2021 13:37
// Description : Intro to Structures in C++
//============================================================================

#include <iostream>
using namespace std;

struct Employee { //define structure
	int id;
	string name;
	string department;
};

int main() {

	Employee employee1 = {12,"fatih kaptan", "kontrol müh."}; //add element in structure
/*or
 *  Employee employee1;
 *  employee1.id = 12;
 *  employee1.name = "fatih kaptan";
 *  employee1.department = "kontrol müh."; *
 */

	cout << employee1.name << endl; //using sub-elements from structures.

	employee1.name = "salih dede" ; //changing sub-elements
	cout << employee1.name << endl;



	return 0;
}
