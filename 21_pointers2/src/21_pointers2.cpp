//============================================================================
// Name        : 21_pointers2.cpp
// Author      : Fatih Kaptan
// Date		   : 19 May 2021 13:36
// Description : Understanding why use pointers?
//============================================================================

#include <iostream>
using namespace std;

void ChangeValue(int af);  //function prototype for "call by value".
void ChangeValue2(int *ptr); //function prototype for "call by reference".


int main() {

//******CALL BY VALUE*********
	int a=10;
	cout << "Value of \"a\" ,firstly: " << a << endl;

	ChangeValue(a);

	cout << "Value of \"a\" ,after function: " << a << endl;

cout << "---------------------------" <<endl;

//*********CALL BY REFERENCE********
	int b=10;
		cout << "Value of \"b\" ,firstly: " << b << endl;

		ChangeValue2(&b); //define adress value of b for using pointer parameters.

		cout << "Value of \"b\" ,after function: " << b << endl;

	return 0;
}


void ChangeValue(int af){
	af=20;
	cout << "Value of \"af\" in function: " << af << endl;
}

void ChangeValue2(int *ptr){
	*ptr = 20; // now, *ptr = b = 20 (changing value using access to variable)
 	cout << "Value of \"*ptr\" in function: " << *ptr << endl;
}

