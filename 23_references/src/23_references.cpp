//============================================================================
// Name        : 23_references.cpp
// Author      : Fatih Kaptan
// Date		   : 19 May 2021 14:36
// Description : References in C++
//============================================================================

#include <iostream>
using namespace std;

void ChangeValue(int &ref); //function prototype

//NOTE: references are watcher of variable.
int main() {



	int a = 10;
	int &ref_1 = a; //Attention! &ref does NOT meaning address of variable.

	ref_1++;
	cout << "new value of a: " << a << endl;

//**Function application**
	int b = 15;
	ChangeValue(b);
	cout << "new value of b: " << b << endl;


	return 0;
}

void ChangeValue(int &ref){
	ref=20;

}

/* Differences between pointers and references:
 *
 * 				int a=10;
 * 	int *ptr;     |   int &ref;
 * 	ptr = &a;	  |	  ref = a;
 * 	  -VALID-	  |   -INVALID-
 *
 *
 */
