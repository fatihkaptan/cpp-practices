//============================================================================
// Name        : 42_encapsulation.cpp
// Author      : Fatih Kaptan
// Date 	   : 25 May 2021 23:17
// Description : Encapsulation in C++
//============================================================================

#include <iostream>
using namespace std;

class Employee {
private:
	int agee;
	string name;
public:

	//string name; //if define public, can't control undefined inputs, we should encapsulate.
	//int agee;

	void setName(string get_Name) {
		name = get_Name;
	}
	string getName() {
		return name;
	}

	void setAge(int input_Age) {
		if (input_Age < 0) {
			cout << "undefined input!" << endl;
		} else {
			agee=input_Age;
		}

	}

	int getAge() {
		return agee;
	}

};

int main() {

	Employee employee1;
	employee1.setName("Fatih");
	employee1.setAge(-2222);

	cout << employee1.getName() << endl;
	cout << "age: " << employee1.getAge() << endl;

	return 0;
}
