//============================================================================
// Name        : 19_functions2.cpp
// Author      : Fatih Kaptan
// Date		   : 17 May 2021 18:48
// Description : Using return; in functions
//============================================================================

#include <iostream>
using namespace std;

int summing(int a, int b, int c){ //define function with return

	//cout << "Sum of terms= " << a+b+c << endl;
	return a+b+c; //if we want integer value as input, we define "int function_name(parameters)"
}

int main() {

/*if we assign a value from any function,
  we can define function as variable(ex:int,double,string)
  and add a "return output_variable;" at the end of the function block.
 */
	int a = summing(1,2,3);

	cout << "a: " << a << endl;
	return 0;
}
