//============================================================================
// Name        : 06_strings.cpp
// Author      : Fatih Kaptan
// Date   	   : 16 May 2021 14:22
// Description : Strings in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

	string  str1 = "this is a string";
	string  str2 = "this is also a string";

	string str3 = str1+str2;

	cout << "1st string : " << str1 <<endl;
	cout << "2nd string : " << str2 <<endl;
	cout << "3rd string : " << str3 <<endl;




	return 0;
}
