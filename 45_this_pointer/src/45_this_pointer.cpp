//============================================================================
// Name        : 45_this_pointer.cpp
// Author      : Fatih Kaptan
// Date 	   : 25 May 2021 23:59
// Description : "This pointers" in C++
//============================================================================

#include <iostream>
using namespace std;

class Employee {
private:
	string name;
	int salary;
	int id;

public:
//use this pointer for using same variable names
	Employee(string name, int salary, int id) {
		this->name = name;
		this->salary = salary;
		this->id = id;
	}

	void setName(string get_Name) {
		name = get_Name;
	}
	string getName() {
		return name;
	}

	void setSalary(int get_Salary) {
		salary = get_Salary;
	}

	int getSalary() {
		return salary;
	}

	void showInfos() {
		cout << "Id: " << this->id << endl;
		cout << "Name: " << this->name << endl;
		cout << "Salary: " << this->salary << endl;
	}

};

int main() {

	Employee employee("Fatih", 2500, 587); //use constructor

	employee.showInfos();

	return 0;
}
