//============================================================================
// Name        : 07_if_else.cpp
// Author      : Fatih Kaptan
// Date 	   : 16 May 2021 14:27
// Description : If, else statements in C++
//============================================================================

//note: same as C
#include <iostream>
using namespace std;

int main() {

	string password = "fatihkaptan";
	string input;

	cout << "please enter the password: ";
	cin >> input;

	if(password==input){
		cout << "password correct!";
	}
	else{
		cout << "password incorrect!";
	}



	return 0;
}
