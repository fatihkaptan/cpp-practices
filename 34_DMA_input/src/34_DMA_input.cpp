//============================================================================
// Name        : 34_DMA_input.cpp
// Author      : Fatih Kaptan
// Date	 	   : 20 May 2021 14:44
// Description : Get input for DMA in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int *ptr;
	int size;

	cout << "Enter size for pointer: " ;
	cin >> size;

	ptr = new int[size];

	for(int i=0; i<size ; i++){
		cout << "Enter Value: ";
		cin >> ptr[i];
	}

	for(int i=0; i<size ; i++){
			cout << "Value = " << ptr[i] <<endl;
	}

	delete [] ptr;

	for(int i=0; i<size ; i++){
				cout << "Value(After delete) = " << ptr[i] <<endl;
		}




	return 0;
}
