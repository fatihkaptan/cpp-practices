//============================================================================
// Name        : 25_const_value.cpp
// Author      : Fatih Kaptan
// Date		   : 19 May 2021 15:00
// Description : Const variables in C++
//============================================================================

#include <iostream>
using namespace std;
//NOTE: Do NOT change values which defined as a "const".
int main() {

	const int i = 32;
	const string j[] = {"fatih","kaptan"};

	//i=40; //error(read-only variable)
	//j[1] = "sabri";
	cout << i << endl;
	cout <<j[1] <<endl;



	return 0;
}
