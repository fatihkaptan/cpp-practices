//============================================================================
// Name        : 46_constructor_overloading.cpp
// Author      : Fatih Kaptan
// Date 	   : 31 May 2021 13:13
// Description : Constructor overloading in C++
//============================================================================

#include <iostream>
using namespace std;

class Employee {
private:
	string name;
	int salary;
	int id;

public:
	Employee() {   //add empty construct
		this->name = "No info!"; //default name
		this->salary = 0; //default salary
		this->id = 0; //default id
		//note if not define default parameters for empty construct, c++ define by itself.
	}

	Employee(string name, int salary) {  //add another construct
		this->name = name;
		this->salary = salary;
		this->id = 0; //default id
	}

	Employee(string name, int salary, int id) {
		this->name = name;
		this->salary = salary;
		this->id = id;
	}
	void showInfos() {
		cout << "Id: " << this->id << endl;
		cout << "Name: " << this->name << endl;
		cout << "Salary: " << this->salary << endl;
	}

};

int main() {

	Employee employee1("Fatih Kaptan", 3300, 12);
	Employee employee2; //using empty construct
	Employee employee3("Sabri Abi", 4300); //using another construct

	employee1.showInfos();
	cout << "-----------" << endl;
	employee2.showInfos();
	cout << "-----------" << endl;
	employee3.showInfos();

	return 0;
}
