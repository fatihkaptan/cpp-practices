//============================================================================
// Name        : 05_cin_input.cpp
// Author      : Fatih Kaptan
// Date   : 16 May 2021 14:10
// Description : Using user input in C++
//============================================================================

#include <iostream>
using namespace std;

/* note!:
 * "<<" meaning output in cpp
 * ">>" meaning input in cpp
 */

int main() {

/*
	int x;
	cout << "Enter a number: ";
	cin >> x;

	cout << "the number: " << x << endl;
*/

//input in order

/*
	int a,b,c;
	cout << "First number: ";
	cin >> a;

	cout << "Second number: ";
	cin >> b;

	cout << "Third number: ";
	cin >> c;

	cout << "Sum of these numbers= " << a+b+c <<endl;
*/

//input in order(single)

	int a,b,c;
	cout << "enter the 3 number: ";
	cin >> a >> b >> c;					//default seperator = one gap
	cout << "sum of these= " << a+b+c;


	return 0;
}
