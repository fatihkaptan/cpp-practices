//============================================================================
// Name        : intro1.cpp
// Author      : Fatih Kaptan
// Date		   : 16 May 2021 13:00
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	return 0;
}
