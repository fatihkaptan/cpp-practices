//============================================================================
// Name        : 26_const_withPointers.cpp
// Author      : Fatih Kaptan
// Date		   : 19 May 2021 15:13
// Description : Using const variables with pointers in C++
//============================================================================

#include <iostream>
using namespace std;

void printArray(const int *ptr1, const int *ptr2); //funciton protype

int main() {

	int array[] = {10,20,30,40,50,60,70,80,90,100};

	printArray(array+2,array+7); //30,80's address


	return 0;
}

void printArray(const int *ptr1, const int *ptr2){
	for( ;ptr1 != ptr2 ;ptr1++){ //condition= break if ptr1=ptr2
		cout << "address: " << ptr1 << "  value= " << *ptr1 <<endl;
	}
	/*attention: pointers address changeable
	  but value on this address do NOT change 'cause const defining.
	So, ptr is changeable but *ptr is NOT changeable.
	*/
}
