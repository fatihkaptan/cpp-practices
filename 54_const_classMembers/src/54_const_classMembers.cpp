//============================================================================
// Name        : 54_const_classMembers.cpp
// Author      : Fatih Kaptan
// Date 	   : 31 May 2021 14:39
// Description : Const class members and methods in C++
//============================================================================

#include <iostream>
using namespace std;

class Test {
private:
	int a;
public:

	Test(int a) {
		this->a = a;
	}
	//add const to end
	int getValue() const { //this method do NOT change "a", and call only const methods.

		//this->a = 20; //error
		//test(); //error
		test2();
		return this->a;
	}

	void test() {
		cout << "Test method" << endl;
	}

	void test2() const{
			cout << "Test const method" << endl;
		}

	void test_Ref(const Test &test_Ref){
		cout <<":call by reference:" << test_Ref.getValue() <<endl;
	}

};

int main() {

/*	 const Test test1(10);
	 test1.a = 20;    //error
	 cout << test1.a<< endl;
*/

	Test test1(10);
	cout << test1.getValue() << endl;


	test1.test_Ref(test1);
	return 0;
}
