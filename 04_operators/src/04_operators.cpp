//============================================================================
// Name        : 04_operators.cpp
// Author      : Fatih Kaptan
// Date   : 16 May 2021 13:52
// Description : Operators in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

	/*
	  Mathematical Operators:

*	 Basics          	 	+, -, *, / <> +=, -=, *=, /=
*	 Incerement operator 	++
*	 Decremant operator		 --

	 */



	int num1 = 10;
	int num2 = 4;

	cout << "Number1 + Number2 = " << num1 + num2 << endl;
	cout << "Number1 - Number2 = " << num1 - num2 << endl;
	cout << "Number1 * Number2 = " << num1 * num2 << endl;
	cout << "Number1 / Number2 = " << float(num1) / num2 << endl; //convert float to get real value


	cout << "\n\n-------------------------------\n\n";

	int a=5,b=10,c=15,d=20;
	cout << "value of a = " << a << endl;
	cout << "value of b = " << b << endl;
	cout << "value of c = " << c << endl;
	cout << "value of d = " << d << endl;

	a += 2;
	b -= 2;
	c++;
	d--;

	cout << "new value of a = " << a << endl;
	cout << "new value of b = " << b << endl;
	cout << "new value of c = " << c << endl;
	cout << "new value of d = " << d << endl;




	return 0;
}
