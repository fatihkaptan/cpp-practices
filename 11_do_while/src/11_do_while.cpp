//============================================================================
// Name        : 11_do_while.cpp
// Author      : Fatih Kaptan
// Date		   : 16 May 2021 16:04
// Description : Do-While loops in C++
//============================================================================

#include <iostream>
using namespace std;
//note: do-block run at least one times
int main() {

	string password = "123456";
	string input;

	do {
		cout << "Enter the password: ";
		cin >> input;
		if (input != password) {
			cout << "wrong password" << endl;
		}

	} while (input != password);

	cout << "correct" << endl;

	return 0;
}
