//============================================================================
// Name        : 41_Private_access_specifier_OOP.cpp
// Author      : Fatih Kaptan
// Date 	   : 21 May 2021 13:46
// Description : Private access specifier in C++
//============================================================================

#include <iostream>
using namespace std;

class Employee {
private: //if define private, u can access only in class operations.
	string name;

public:
	//we define public functions to access private variables.
	void setName(string get_Name) {
		name = get_Name;
	}
	string getName() {
		return name;
	}

};

int main() {

	Employee employee1;
	employee1.setName("Fatih");

	cout << employee1.getName();

	return 0;
}
