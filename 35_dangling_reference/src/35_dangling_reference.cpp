//============================================================================
// Name        : 35_dangling_reference.cpp
// Author      : Fatih Kaptan
// Date	  	   : 21 May 2021 12:42
// Description : Null pointer, Dangling reference and pointers practices in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int *ptr = nullptr; //define pointers as a null pointer more safe.
	cout << ptr << endl;

	ptr = new int;
	cout << ptr << endl;

	delete ptr; //forward address to safe address -> dangling reference
	cout << ptr << endl;

	*ptr = 10; //runtime error(different platforms)
 	cout << ptr << endl;

 	//***********************
 	int *ptr1 = nullptr;
 	int *ptr2 = nullptr;

 	ptr1 = new int;

 	*ptr1 = 10;
 	ptr2 = ptr1;

 	cout << ptr1 <<" : " << ptr2 << endl;

 	delete ptr1;

 	cout << ptr1 <<" : " << ptr2 << endl;


	return 0;
}
