//============================================================================
// Name        : 20_pointers.cpp
// Author      : Fatih Kaptan
// Date		   : 19 May 2021 12:59
// Description : Intro to Pointers in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int a =5;

/*
	  an integer cover 4 bytes, one block is 1 byte,
	   so an integer cover 4 bytes in memory ex: |1001.||1002.||1003.||1004.|
	   so &a is adress of variable like 1001,1002... (in hex)
 */

	cout << "Adress of variable a : " << &a <<endl;


	int *ptr = &a; //assign the adress of "a" variable to "ptr",
				  //also "*ptr" equal to value of "a"

	cout << "Adress of variable a, using pointer : " << ptr <<endl;
	cout << "Value of variable a, using pointer: " << *ptr << endl;

	*ptr = 6; //assign new value to "a" ,using pointer

	cout << "new value of a: " << a << endl;


	return 0;
}
