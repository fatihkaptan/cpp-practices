//============================================================================
// Name        : 30_nested_structures.cpp
// Author      : Fatih Kaptan
// Date	  	   : 20 May 2021 14:01
// Description : Nested Structures in C++
//============================================================================

#include <iostream>
using namespace std;

struct Address {
	string cityName;
	int No;
};

struct Employee {
	int id;
	string name;
	string department;
	Address address;	 // <--- NESTED defined
};

int main() {
	Employee employee = {12,"fatih kaptan" ,"kontrol müh", {"Izmir",28}};

	/*or
	 * Employee employee;
	 * employee.address = {"Izmır",28};
	 * 			or
	 * employee.address.cityName = "Izmır";
	 * employee.address.no = 28;
	 */

	cout << employee.id << endl;
	cout << employee.name << endl;
	cout << employee.department << endl;
	cout << employee.address.cityName << endl;
	cout << employee.address.No << endl;



	return 0;
}
