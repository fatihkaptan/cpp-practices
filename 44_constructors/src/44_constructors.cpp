//============================================================================
// Name        : 44_constructors.cpp
// Author      : Fatih Kaptan
// Date 	   : 25 May 2021 23:49
// Description : Constructors in C++
//============================================================================

#include <iostream>
using namespace std;

class Employee {
private:
	string name;
	int salary;
	int id;

public:

	Employee(string isim,int maas, int kimlik){//define constructor
		name = isim;
		salary= maas;
		id = kimlik;
	}

	void setName(string get_Name) {
		name = get_Name;
	}
	string getName() {
		return name;
	}

	void setSalary(int get_Salary){
		salary=get_Salary;
	}

	int getSalary(){
		return salary;
	}

	void showInfos(){
		cout << "Id: " << id <<endl;
		cout << "Name: " << name <<endl;
		cout << "Salary: " <<salary <<endl;
	}

};

int main() {

	Employee employee("Fatih",2500,587); //use constructor

	//new

	employee.showInfos();


/* old
	employee.setName("Fatih");
	employee.setSalary(25000);

	cout <<employee.getName() <<endl;
	cout <<employee.getSalary() <<endl;
*/





	return 0;
}
