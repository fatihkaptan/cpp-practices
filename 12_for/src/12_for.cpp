//============================================================================
// Name        : 12_for.cpp
// Author      : Fatih Kaptan
// Date		   : 16 May 2021 16:14
// Description : For loops in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {
//for(control_variable ; condition ; manipulation of control_variable)


//int i; //also defining in for_loop parameters. Only Cpp

	/*	for(int i=1 ; i<10 ; i++){
	 cout << "i: " <<  i << endl;
	 }
	 */


//FACTORIAL EXAMPLE
	int num;
	cout << "Enter Number: ";
	cin >> num;
	//5! = 5*4*3*2*1
	int factorial = 1;

	for (int i = 1; i <= num; i++) {
		factorial *= i;

	}

	cout << "factorial= " << factorial << endl;

	return 0;
}
