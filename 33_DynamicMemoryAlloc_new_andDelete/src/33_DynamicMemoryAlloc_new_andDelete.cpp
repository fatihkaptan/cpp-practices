//============================================================================
// Name        : 33_DynamicMemoryAlloc_new_andDelete.cpp
// Author      : Fatih Kaptan
// Date		   : 20 May 2021 14:33
// Description : Intro to Dynamic Memory Allocation in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

/*	int *ptr;              //automatic memory alloc.
	int a[] = {1,2,3,4,5};
	ptr=a;
	cout << ptr[1] <<endl;
*/
	int *ptr = new int; // manually add new memory for integer value with "ptr" pointer on memory.

	cout << *ptr << endl; //randomly value when NOT assign any value to pointer.
	cout << ptr << endl;

	*ptr = 5;
	cout << "---next---" << endl;
	cout << *ptr << endl; //work
	cout << ptr << endl;

	cout << "---next---" << endl;
	delete ptr;
	cout << *ptr << endl; //randomly value
	cout << ptr << endl;




	return 0;
}
