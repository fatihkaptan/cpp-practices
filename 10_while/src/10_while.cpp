//============================================================================
// Name        : 10_while.cpp
// Author      : Fatih Kaptan
// Date 	   : 16 May 2021 15:55
// Description : While loops in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int i=1, j=1;

	while(i<10 && j<5){
		cout << "i: " << i << "---" << " j: " << j << endl;
		i+=2, j++;
	}


	return 0;
}
