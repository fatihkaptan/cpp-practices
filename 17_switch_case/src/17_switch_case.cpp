//============================================================================
// Name        : 17_switch_case.cpp
// Author      : Fatih Kaptan
// Date		   : 17 May 2021 17:55
// Description : Switch-Case statement in C++
//============================================================================

#include <iostream>
using namespace std;

//Note: All of case's must be include break;
//If don't use, other case statement run even if not called!!
int main() {

	int operation;

	cout << "Enter the Op. -> ";
	cin >> operation;
//	cout << "Operation: " << operation << endl;

	switch (operation) {
	case 1:
		cout << "1st op." <<endl;
		break;
	case 2:
		cout << "2nd op." <<endl;
		break;

	case 3:
		cout << "3rd op." <<endl;
		break;

	case 4:
		cout << "4th op." <<endl;
		break;
	default:
		cout << "Unknown!" << endl;
	}

	return 0;
}
