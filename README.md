YazılımBilimi Youtube kanalı üzerinden takip ettiğim Mustafa Murat Coşkun tarafından verilen “Temel C++ Dersleri” Kod Parçacıkları

https://fatihkaptan.tk/genel/cpp-snippets/
https://www.youtube.com/playlist?list=PLIHume2cwmHfmSmNlxXw1j9ZAKzYyiQAq

--------------------------------------------------------------------------------
1. Hello World in C++
2. Variable types in C++
3. Operators in C++
4. Using user input in C++
5. Strings in C++
6. If, else statements in C++
7. If, else if , else statements in C++
8. Logical Operators in C++
9. While loops in C++
10. Do-While loops in C++
11. For loops in C++
12. Break and Continue statements in C++
13. User Login Example with break,loops,statements...
14. Arrays in C++
15. Multidimensional Arrays in C++
16. Switch-Case statement in C++
17. Intro to Functions in C++
18. Using return; in functions
19. Intro to Pointers in C++
20. Understanding why use pointers?
21. Pointer aritmetics and using arrays with pointers in C++
22. References in C++
23. Sending arrays to functions example in C++
24. Const variables in C++
25. Using const variables with pointers in C++
26. Intro to Structures in C++
27. Using pointers with structures in C++
28. Using structures with functions in C++
29. Nested Structures in C++
30. Using Nested Structures with Pointers in C++
31. Using sizeof operator in C++
32. Intro to Dynamic Memory Allocation in C++
33. Get input for DMA in C++
34. Null pointer, Dangling reference and pointers practices in C++
35. For(Each) loops in C++
36. Function overloading in C++
37. Intro to Object oriented programming in C++
38. Class methods in C++
39. Header files and implemenatitons in C++
40. Private access specifier in C++
41. Encapsulation in C++
42. Relation between pointers and classes in C++
43. Constructors in C++
44. this-> pointers in C++
45. Constructor overloading in C++
46. Understanding pointers with classes in C++
47. Destructors in C++
48. Destructors example and Memory Leak in C++
49. Static Variables in C++
50. Static class members in C++
51. Static Class Methods in C++
52. Friend functions and classes in C++
53. Const class members and methods in C++
