//============================================================================
// Name        : 15_arrays.cpp
// Author      : Fatih Kaptan
// Date		   : 17 May 2021 17:12
// Description : Arrays in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

	//**********S1********
/*
	int array[3]; //this array has 3 variable= array[0],[1],[2]
	array[0]=10;
	array[1]=20;
	array[2]=30;

	cout << "0th index: " << array[0] << endl;
	cout << "1st index: " << array[1] << endl;
	cout << "2nd index: " << array[2] << endl;
*/

	//**********S2********


	//int array[]={10,20,30,40};

/*
	double array2[]= {1.2, 3.5, 4.6, 7.6};

	for(int i=0 ; i<4 ; i++){
		cout << i << ". index value: " << array2[i] << endl;
	}
*/

	//**********S3********

	string array3[4];
	for(int i=0 ; i<4 ; i++){
		cout << "enter the " << i << ". value: ";
		cin >> array3[i];
	}

	for(int i =0 ; i<4 ; i++){
		cout << i << ". index value: " << array3[i] << endl;
	}


	return 0;
}
