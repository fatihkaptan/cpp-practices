//============================================================================
// Name        : 22_pointer_arithmetic_andArrays.cpp
// Author      : Fatih Kaptan
// Date 	   : 19 May 2021 13:59
// Description : Pointer aritmetics and usin arrays with pointers in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int array[] = {1,2,3,4};

	cout << array[1] << endl;
	cout << array[3] << endl;

	cout << array << endl; //show adress of start point of array.
	cout << array+1 << endl;//4 byte more, 'cause int value cover 4 byte, so address+1 meaning next integer on memory

	cout << "-----------" <<endl;

	int *ptr = array; //assign starting address of array on memory
	cout << ptr << endl;

	ptr = ptr+1;
	cout << "new value of \"ptr\": " << ptr <<endl;

	int *ptr2 = &array[2]; //assign 2nd index adress of array to ptr2.
	cout << "ptr2 = " << ptr2 << endl;

	cout << "-----------" <<endl;

	string array2[] = {"Fatih","Kapitan","2021"};
	string *ptr3 = array2;
	cout << "ptr3= " << ptr3 <<endl;

	ptr3 = ptr3 + 1;
	cout << "ptr3 +1 = " << ptr3 <<endl; //string variable cover 20 byte on memory
	cout << "*(ptr+1)= " << *ptr3 <<endl; //calling value using address

/* NOTE
 *
 *	string array = {"a","b","c"} ,  *ptr = &array
 *
 *	array[0] = a | *ptr     = a  | ptr   = 0x..00
 *	array[1] = b | *(ptr+1) = b  | ptr+1 = 0x..20
 *	array[2] = c | *(ptr+2) = c  | ptr+2 = 0x..40
 *
 */
	return 0;
}
