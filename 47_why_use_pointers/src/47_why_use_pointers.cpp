//============================================================================
// Name        : 47_why_use_pointers.cpp
// Author      : Fatih Kaptan
// Date 	   : 31 May 2021 13:27
// Description : Understanding pointers in C++
//============================================================================

#include <iostream>
using namespace std;

class Employee {
private:
	string name;
	int salary;
	int id;

public:
	Employee(string name, int salary, int id) {
		this->name = name;
		this->salary = salary;
		this->id = id;
	}
	void showInfos() {
		cout << "Id: " << this->id << endl;
		cout << "Name: " << this->name << endl;
		cout << "Salary: " << this->salary << endl;
	}

};

void func1(Employee emp){
	emp.showInfos();
}

void func2(Employee* ptr){
	ptr->showInfos();

}



int main() {

	Employee employee("Fatih Kaptan",3300,587);

	//employee.showInfos();
	func1(employee); //call by value ( create "emp" as a  extra object)
cout << "-------------"<<endl;
	func2(&employee); //call by reference ( use pointer, save memory :) )

	return 0;
}
