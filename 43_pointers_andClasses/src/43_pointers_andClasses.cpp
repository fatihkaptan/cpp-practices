//============================================================================
// Name        : 43_pointers_andClasses.cpp
// Author      : Fatih Kaptan
// Date 	   : 25 May 2021 23:40
// Description : Relation between pointers and classes in C++
//============================================================================

#include <iostream>
using namespace std;

class Employee {
private:
	int age;
	string name;
public:

	void setName(string get_Name) {
		name = get_Name;
	}

	string getName() {
		return name;
	}

	void setAge(int yas) {
		if (yas < 0) {
			cout << "undefined input!" << endl;
		} else {
			age=yas;
		}
	}

	int getAge() {
		return age;
	}

};

int main() {

	Employee *employee = new Employee(); //using as pointer

	employee->setName("Fatih");
	employee->setAge(55);

	cout << employee->getName() << endl;
	cout << employee->getAge() << endl;

	return 0;
}
