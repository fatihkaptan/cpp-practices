//============================================================================
// Name        : 37_function_overloading.cpp
// Author      : Fatih Kaptan
// Date 	   : 21 May 2021 13:00
// Description : Function overloading in C++
//============================================================================

#include <iostream>
using namespace std;

//you can define same function again with different parameters.

void sayHi(){ //1
	cout << "Helloo!" << endl;
}

void sayHi(string name){ //2
	cout << "Helloo " << name <<endl;
}

void sayHi(string name, string surname){ //3
	cout << "Helloo " << name << surname << endl;
}

int main() {
	sayHi(); //work 1
	sayHi("Fatih"); //work 2
	sayHi("Fatih","Kaptan"); //work3

	return 0;
}
