//============================================================================
// Name        : 13_break_continue.cpp
// Author      : Fatih Kaptan
// Date 	   : 16 May 2021 16:29
// Description : Break and Continue statements in C++
//============================================================================

#include <iostream>
using namespace std;

int main() {
// The break statement ends the loop immediately when it is encountered.
//The continue statement skips the current iteration of the loop and continues with the next iteration.

//BREAK

/*
	int i=0;

	while(i<10){
		if(i==5){
			break;
		}
		cout << "i: " << i <<endl;
		i++;
	}
*/

//CONTINUE
/*
	for(int i=0 ; i<10 ; i++){
		if(i==3 || i==5){
			continue;
		}
		cout << "i: " << i << endl;
	}
	*/

//CONTINUE IN WHILE

	int i=0;

	while(i<10){

		if(i==3 || i==5){
			i++; 			 //attention,infinite loop if use below continue
			continue;
		}
		cout << "i: " << i << endl;
		i++;
	}

	return 0;


}
